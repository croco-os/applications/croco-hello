croco-hello
=============

A tool providing access to documentation and support for new Croco users.

## What is Croco Hello?

Croco-hello is widely inspired by [croco-welcome](https://github.com/croco-os/croco-welcome).
Currently, croco-hello has all the major features of croco-welcome plus a translation system.
- Interface is translated using gettext and po files (po/).
- Pages are translated using differents files (data/pages).

## What goals ?

The goal of the project is to build a powerful user interface that allows the user to discover his favorite distribution :).

## Technologies

Croco-hello is build with Python, Gtk3 and Glade.

## TODO

- Make more translations to distribute it in all the world.

Let's make a wonderful software !
